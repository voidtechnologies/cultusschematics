package com.voidtechnologies.cultusschematics.listeners;

import com.voidtechnologies.cultusschematics.PointSet;
import com.voidtechnologies.cultusschematics.RegionManager;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import static org.bukkit.event.block.Action.LEFT_CLICK_BLOCK;
import static org.bukkit.event.block.Action.RIGHT_CLICK_BLOCK;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 *
 * @author Dylan Feb 13, 2019
 */
public class CSListener implements Listener {

    @EventHandler()
    public void rodClick(PlayerInteractEvent event) {
        Player player = event.getPlayer();

        if (event.getItem() == null || !event.getItem().getType().equals(Material.STICK)) {
            return;
        }

        PointSet points = RegionManager.getPlayerPoints(player.getUniqueId());

        if (points == null) {
            points = new PointSet();
        }
        if (event.getAction().equals(LEFT_CLICK_BLOCK)) {
            points.setPointA(event.getClickedBlock().getLocation());
            player.sendMessage(ChatColor.BLUE + "Set 1st position to " + locToString(points.getPointA()) + ".");
        } else if (event.getAction().equals(RIGHT_CLICK_BLOCK)) {
            points.setPointB(event.getClickedBlock().getLocation());
            player.sendMessage(ChatColor.BLUE + "Set 2nd position to " + locToString(points.getPointB()) + ".");
        }
        RegionManager.putPlayerPoints(player.getUniqueId(), points);
        
        player.sendMessage(ChatColor.BLUE + "Selection volume is " + (points.getVolume()));
    }
    
    public String locToString(Location loc) {
        return "(" + loc.getBlockX() + ", " + loc.getBlockY() + ", " + loc.getBlockZ() + ")";
    }
}
