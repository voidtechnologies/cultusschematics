package com.voidtechnologies.cultusschematics.commands;

import com.voidtechnologies.cultusschematics.CultusSchematics;
import com.voidtechnologies.cultusschematics.PointSet;
import com.voidtechnologies.cultusschematics.RegionManager;
import com.voidtechnologies.cultusschematics.Schematic;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

/**
 *
 * @author Dylan Feb 13, 2019
 */
public class SchematicCommand extends CultusCommand {

    public SchematicCommand() {
        super("cschem");
    }

    private static HashMap<String, Integer> palette = new HashMap<>();
    private static HashMap<Integer, String> rPal = new HashMap<>();
    private static HashMap<Integer, String[]> entityMap = new HashMap<>();

    private static short blocks[];

    @Override
    public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] strings) {
        if (!(cs instanceof Player)) {
            return false;
        }
        Player player = (Player) cs;

        PointSet points = RegionManager.getPlayerPoints(player.getUniqueId());

        if (points.getVolume() == 0) {
            player.sendMessage(ChatColor.RED + "You must make two point selections in order to copy/paste a region!");
            return true;
        }

        int width = points.getXDist();
        int height = points.getYDist();
        int length = points.getZDist();

        Location aLoc = points.getPointA().clone(); //corner 1
        Location bLoc = points.getPointB().clone(); //corner 2
        Location oLoc;

        byte ret = CultusSchematics.compareCorners(aLoc, bLoc);

        switch (ret) {
            case 1:
                oLoc = aLoc;
                break;
            case -1:
                oLoc = bLoc;
                break;
            default:
                if (aLoc.getBlockX() <= bLoc.getBlockX()) {
                    oLoc = aLoc;
                    oLoc.setZ(bLoc.getZ());
                } else {
                    oLoc = bLoc;
                    oLoc.setZ(aLoc.getZ());
                }
                break;
        }
        oLoc.setY(aLoc.getBlockY() <= bLoc.getBlockY() ? aLoc.getY() : bLoc.getY());

        if (strings[0].equals("copy")) {
            palette.clear();
            rPal.clear();
            entityMap.clear();

            Block origin = oLoc.getBlock();

            blocks = new short[points.getVolume()];
            int paletteSize = 0;
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    for (int z = 0; z < length; z++) {
                        Block b = origin.getRelative(x, y, z);
                        int index = y * width * length + z * width + x;

                        if (b.getType().equals(Material.LEGACY_WALL_SIGN) || b.getType().equals(Material.LEGACY_SIGN)) {
                            System.out.println("Is sign");
                            extractSignData((Sign) b.getState(), index);
                        }
                        String data = b.getBlockData().getAsString();
                        if (!palette.containsKey(data)) {
                            palette.put(data, paletteSize++);
                        }
                        blocks[index] = palette.get(data).shortValue();
                    }
                }
            }
            for (Entry<String, Integer> entry : palette.entrySet()) {
                rPal.put(entry.getValue(), entry.getKey());
                System.out.println(entry.getKey() + ": " + entry.getValue());
            }
            System.out.println("\n\n" + Arrays.toString(blocks));

        } else if (strings[0].equals("paste")) {
            Block origin = player.getLocation().getBlock();
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    for (int z = 0; z < length; z++) {
                        int index = y * width * length + z * width + x;
                        Block b = origin.getRelative(x, y, z);
                        short id = blocks[index];
                        String data = rPal.get((int) id);
                        b.setBlockData(Bukkit.createBlockData(data));
                        String[] lData = entityMap.get(index);
                        if (lData != null) {
                            updateSignData((Sign) b.getState(), lData);
                        }
                    }
                }
            }
        } else if (strings[0].equals("save")) {
            String name = strings[1];
            File f = new File(CultusSchematics.getInstance().getDataFolder() + "\\schematics\\" + name + ".cschem");
            File folder = new File(CultusSchematics.getInstance().getDataFolder() + "\\schematics\\");

            if (!f.exists()) {
                try {
                    if (!folder.exists()) {
                        folder.mkdirs();
                    }
                    f.createNewFile();
                } catch (IOException ex) {
                    Logger.getLogger(SchematicCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            YamlConfiguration cfg = YamlConfiguration.loadConfiguration(f);

            ConfigurationSection section = cfg.createSection("schematic");

            section.set("width", width);
            section.set("length", length);
            section.set("height", height);

            section.set("blocks", blocks);

            ConfigurationSection pSec = section.createSection("palette");
            for (Entry<Integer, String> entry : rPal.entrySet()) {
                pSec.set(entry.getKey().toString(), entry.getValue());
            }

            ConfigurationSection sSec = section.createSection("signData");

            for (Entry<Integer, String[]> entry : entityMap.entrySet()) {
                sSec.set(entry.getKey().toString(), entry.getValue());
            }

            try {
                cfg.save(f);
            } catch (IOException ex) {
                Logger.getLogger(SchematicCommand.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (strings[0].equals("load")) {
            Schematic schem = new Schematic(strings[1]);
            
            schem.paste(player.getLocation());
        }
        return super.onCommand(cs, cmnd, string, strings);
    }

    private void updateSignData(Sign sign, String[] lines) {
        for (int i = 0; i < lines.length; i++) {
            String s = lines[i];
            System.out.println("L" + i + ": " + s);
            sign.setLine(i, s);
        }
        sign.update();
    }

    private void extractSignData(Sign sign, int index) {
        entityMap.put(index, sign.getLines());
    }
}