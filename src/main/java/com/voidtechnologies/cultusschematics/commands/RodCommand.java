package com.voidtechnologies.cultusschematics.commands;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author Dylan
 * Feb 13, 2019
 */
public class RodCommand extends CultusCommand {

    public RodCommand() {
        super("rod");
    }

    @Override
    public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] strings) {
        if (!(cs instanceof Player)) {
            return false;
        }
        
        Player player = (Player) cs;
        
        if (player.getInventory().contains(Material.STICK)) {
            return true;
        }
        
        player.getInventory().addItem(new ItemStack(Material.STICK));
        return super.onCommand(cs, cmnd, string, strings);
    }
}