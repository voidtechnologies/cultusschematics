
package com.voidtechnologies.cultusschematics.commands;

import com.voidtechnologies.cultusschematics.CultusSchematics;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Dylan
 * Feb 13, 2019
 */
public class PasteCommand extends CultusCommand {

    public PasteCommand() {
        super("cpaste");
    }

    @Override
    public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] strings) {
        CultusSchematics.schem.paste(((Player) cs).getLocation());
        return super.onCommand(cs, cmnd, string, strings); 
    }
}