package com.voidtechnologies.cultusschematics;

import java.util.Collection;
import java.util.HashMap;
import java.util.UUID;

/**
 *
 * @author Dylan Feb 13, 2019
 */
public class RegionManager {

    private static HashMap<UUID, PointSet> points = new HashMap<>();

    public static PointSet getPlayerPoints(UUID o) {
        return points.get(o);
    }

    public static  boolean playerHadPoints(UUID o) {
        return points.containsKey(o);
    }

    public static  PointSet putPlayerPoints(UUID k, PointSet v) {
        return points.put(k, v);
    }

    public static  PointSet removePlayerPoints(UUID o) {
        return points.remove(o);
    }

    public static  void clear() {
        points.clear();
    }

    public static  Collection<PointSet> values() {
        return points.values();
    }
}