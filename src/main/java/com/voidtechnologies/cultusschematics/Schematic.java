package com.voidtechnologies.cultusschematics;

import com.voidtechnologies.cultus.civilizations.Nation;
import com.voidtechnologies.cultus.managers.LuxuryManager;
import com.voidtechnologies.cultus.runnables.ConstructionRunnable;
import com.voidtechnologies.cultus.structures.Constructure;
import com.voidtechnologies.cultus.structures.Position2d;
import com.voidtechnologies.cultus.structures.Position3d;
import com.voidtechnologies.cultus.structures.Structure;
import com.voidtechnologies.cultus.structures.StructureType;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.block.data.BlockData;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 *
 * @author Dylan Mar 1, 2019
 */
public class Schematic {

    private HashMap<Integer, String[]> signData;
    private HashMap<Integer, String> palette;
    private int width, height, length;
    private short blocks[];

    public Schematic(String file) {
        this(new File(CultusSchematics.getInstance().getDataFolder() + "\\schematics\\" + file + ".cschem"));
    }

    public Schematic(File file) {
        System.out.println("Loading schematic: " + file.getName());
        setupSchematic(YamlConfiguration.loadConfiguration(file));
    }

    private void setupSchematic(YamlConfiguration file) {
        ConfigurationSection section = file.getConfigurationSection("schematic");
        if (section == null) {
            System.out.println("nulllll");
            return;
        }

        width = section.getInt("width");
        length = section.getInt("length");
        height = section.getInt("height");

        int vol = width * length * height;
        Short[] temp = new Short[vol];
        blocks = new short[vol];
        section.getShortList("blocks").toArray(temp);

        for (int i = 0; i < vol; i++) {
            blocks[i] = temp[i];
        }

        // custom retrieval for maps
        palette = new HashMap<>();
        section.getConfigurationSection("palette").getKeys(false).forEach((key) -> {
            palette.put(Integer.parseInt(key), section.getString("palette." + key));
        });

        signData = new HashMap<>();
        section.getConfigurationSection("signData").getKeys(false).forEach((key) -> {
            List<String> sList = section.getStringList("signData." + key);
            String[] text = new String[sList.size()];

            for (int i = 0; i < text.length; i++) {
                text[i] = sList.get(i);
            }
            signData.put(Integer.parseInt(key), text);
        });
    }

    public HashMap<Integer, String[]> getSignData() {
        return signData;
    }

    public HashMap<Integer, String> getPalette() {
        return palette;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getLength() {
        return length;
    }

    public short[] getBlocks() {
        return blocks;
    }

    public List<Block> paste(Location loc) {
        List<Block> retBlocks = new ArrayList<>();
        Block origin = loc.getBlock();
        HashMap<Integer, BlockData> postBlocks = new HashMap<>();
        for (int y = 0; y < this.height; ++y) {
            for (int x = 0; x < this.width; ++x) {
                for (int z = 0; z < this.length; ++z) {
                    int index = y * width * length + z * width + x;
                    BlockData bd = Bukkit.createBlockData(palette.get((int) blocks[index]));
                    if (bd.getMaterial().equals(Material.AIR)) {
                        continue;
                    }
                    Block b = origin.getRelative(x, y, z);
                    if (isPostBlock(bd)) {
                        postBlocks.put(index, bd);
                        continue;
                    }
                    b.setBlockData(bd);
                    retBlocks.add(b);
                }
            }
        }
        for (Entry<Integer, BlockData> post : postBlocks.entrySet()) {
            int x, y, z;
            int index = post.getKey();
            y = index / (width * length);
            z = (index % (width * length)) / width;
            x = (index % (width * length)) % width;
            Block pB = origin.getRelative(x, y, z);
            BlockData pBd = post.getValue();
            pB.setBlockData(pBd);
            String[] lines = signData.get(index);
            if (lines != null) {
                updateSignData((Sign) pB.getState(), lines);
            }
            retBlocks.add(pB);
        }
        return retBlocks;
    }

    public void pastePercent(Location loc, double percent) {
        double lHeight = Math.floor((double) height * percent);
        if (percent >= 1.0) {
            lHeight = this.height;
        }

        Block origin = loc.getBlock();
        for (int y = 0; y < lHeight; ++y) {
            for (int x = 0; x < this.width; ++x) {
                for (int z = 0; z < this.length; ++z) {
                    int index = y * width * length + z * width + x;
                    BlockData bd = Bukkit.createBlockData(palette.get((int) blocks[index]));
                    Block b = origin.getRelative(x, y, z);
                    if (isPostBlock(bd)) {
                        continue;
                    }
                    b.setBlockData(bd);
                }
            }
        }
    }

    public boolean isPostBlock(BlockData bd) {
        Material mat = bd.getMaterial();
        String sMat = mat.toString();
        return (mat.isTransparent() || sMat.contains("DOOR") || sMat.contains("WATER") || mat.hasGravity() || sMat.contains("SIGN"));
    }

    private void updateSignData(Sign sign, String[] lines) {
        for (int i = 0; i < lines.length; i++) {
            String s = lines[i];
            System.out.println("L" + i + ": " + s);
            sign.setLine(i, s);
        }
        sign.update();
    }

    public void clearArea(Location loc, boolean construction) {

        for (int y = 0; y <= this.height; ++y) {
            for (int x = -1; x <= this.width; ++x) {
                for (int z = -1; z <= this.length; ++z) {
                    Location l = new Location(loc.getWorld(), x + loc.getX(), y + loc.getY(), z + loc.getZ());
                    if ((x == -1 && z == -1)
                            || (x == this.width && z == this.length)
                            || (x == -1 && z == this.length)
                            || (z == -1 && x == this.width)
                            || (y == 0 && (x == -1 || z == -1))
                            || (y == 0 && (x == this.width || z == this.length))
                            || (y == this.height && (x == -1 || z == -1))
                            || (y == this.height && (x == this.width || z == this.length))) {
                        if (construction) {
                            l.getBlock().setType(Material.BEDROCK);
                        }
                        continue;
                    }
                    if (x < 0 || z < 0 || z == this.length || x == this.width) {
                        continue;
                    }
                    l.getBlock().setType(Material.AIR);
                }
            }
        }
    }

    public void removeConstructionBoundaries(Location loc) {
        for (int y = 0; y <= this.height; ++y) {
            for (int x = -1; x <= this.width; ++x) {
                for (int z = -1; z <= this.length; ++z) {
                    Location l = new Location(loc.getWorld(), x + loc.getX(), y + loc.getY(), z + loc.getZ());

                    if ((x == -1 && z == -1)
                            || (x == this.width && z == this.length)
                            || (x == -1 && z == this.length)
                            || (z == -1 && x == this.width)
                            || (y == 0 && (x == -1 || z == -1))
                            || (y == 0 && (x == this.width || z == this.length))
                            || (y == this.height && (x == -1 || z == -1))
                            || (y == this.height && (x == this.width || z == this.length))) {
                        l.getBlock().setType(Material.AIR);
                    }
                }
            }
        }
    }

    public void eraseSchematic(Location loc) {
        //erase
        for (int x = 0; x < this.width; ++x) {
            for (int y = 0; y < this.height; ++y) {
                for (int z = 0; z < this.length; ++z) {
                    final Location l = new Location(loc.getWorld(), x + loc.getX(), y + loc.getY(), z + loc.getZ());
                    final Block block = l.getBlock();
                    block.setType(Material.AIR);
                }
            }
        }
    }

    public String checkArea(Location loc, Nation civ, StructureType type) {
        double totalBlocks = blocks.length;
        double airBlocks = 0;
        for (int y = 0; y <= this.height; ++y) {
            for (int x = -1; x <= this.width; ++x) {
                for (int z = -1; z <= this.length; ++z) {

                    final Location l = new Location(loc.getWorld(), x + loc.getX(), y + loc.getY(), z + loc.getZ());
                    if (!civ.containsTerritory(Position2d.convertChunkToPosition(l.getChunk()))) {
                        return ChatColor.RED + "Must be built entirely within your own Civilization's territory!";
                    }
                    
                    if (LuxuryManager.getResource(Position2d.convertChunkToPosition(l.getChunk())) != null && !type.equals(StructureType.TRADE_POST)) {
                        return ChatColor.RED + "Cannot build a Structure on a chunk containing a Luxury Resource! Except Trading Posts.";
                    }
                    
                    if (l.getBlock().getType() == Material.AIR) {
                        airBlocks++;
                    }
                    for (Structure struct : civ.getStructures()) {
                        if (struct.getChunks().contains(Position2d.convertChunkToPosition(l.getChunk())) && struct.getBlocks().contains(Position3d.convertBlockToPosition(l.getBlock()))) {
                            return ChatColor.RED + "Cannot intersect another structure!";
                        }
                    }
                    for (ConstructionRunnable sbr : civ.getInProgressBuilds()) {
                        Constructure construct = sbr.getConstructure();
                        if (construct.getChunks().contains(Position2d.convertChunkToPosition(l.getChunk())) && construct.getBlocks().contains(Position3d.convertBlockToPosition(l.getBlock()))) {
                            return ChatColor.RED + "Cannot intersect another structure!";
                        }
                    }
                }
            }
        }

        return ((double) airBlocks / (double) totalBlocks) > .85 ? "" : ChatColor.RED
                + "Building zone must be >= 85% air!";
    }

    public LinkedList<Block> getPastedBlocks(Location loc) {
        LinkedList<Block> retBlocks = new LinkedList<>();
        //paste
        for (int x = 0; x < this.width; ++x) {
            for (int y = 0; y < this.height; ++y) {
                for (int z = 0; z < this.length; ++z) {
                    int index = y * this.width * this.length + z * this.width + x;
                    int b = this.blocks[index] & 0xFF;//make the block unsigned, so that blocks with an id over 127, like quartz and emerald, can be pasted
                    if (b == 0) {
                        continue;
                    }
                    retBlocks.add(new Location(loc.getWorld(), x + loc.getX(), y + loc.getY(), z + loc.getZ()).getBlock());
                }
            }
        }
        return retBlocks;
    }
}