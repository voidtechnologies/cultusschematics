package com.voidtechnologies.cultusschematics;

import com.voidtechnologies.cultusschematics.commands.CultusCommand;
import com.voidtechnologies.cultusschematics.commands.PasteCommand;
import com.voidtechnologies.cultusschematics.commands.RodCommand;
import com.voidtechnologies.cultusschematics.commands.SchematicCommand;
import com.voidtechnologies.cultusschematics.listeners.CSListener;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author Dylan Feb 13, 2019
 */
public class CultusSchematics extends JavaPlugin {

    private static CultusSchematics instance;
    public static Schematic schem;

    @Override
    public void onEnable() {
        instance = this;
        Bukkit.getServer().getPluginManager().registerEvents(new CSListener(), this);
        new SchematicCommand();
        new RodCommand();
        new PasteCommand();
        CultusCommand.registerAllCommands();
        super.onEnable();
        
        System.out.println("Finished Enabling!");
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    public static CultusSchematics getInstance() {
        return instance;
    }

    public static byte compareCorners(Location c, Location o) {
        int cX, oX, cZ, oZ;
        cX = c.getBlockX();
        oX = o.getBlockX();

        cZ = c.getBlockZ();
        oZ = o.getBlockZ();
        
        if (cX <= oX && cZ <= oZ) {
            return 1;
        } else if (oX <= cX && oZ <= cZ) {
            return -1;
        }
        return 0;
    }
}