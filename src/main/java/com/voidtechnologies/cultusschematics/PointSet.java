package com.voidtechnologies.cultusschematics;

import org.bukkit.Location;

/**
 *
 * @author Dylan Feb 13, 2019
 */
public class PointSet {

    private Location a, b;

    public PointSet() {
    }

    public PointSet(Location a, Location b) {
        this.a = a;
        this.b = b;
    }

    public Location getPointA() {
        return a;
    }

    public void setPointA(Location a) {
        this.a = a.clone();
    }

    public Location getPointB() {
        return b;
    }

    public void setPointB(Location b) {
        this.b = b.clone();
    }

    public int getVolume() {
        if (a == null || b == null) {
            return 0;
        }
        int vol =  getXDist() * getYDist() * getZDist();
        System.out.println("Volume: " + vol);
        return vol;
    }

    public int getXDist() {
        if (a == null || b == null) {
            return 0;
        }
        return Math.abs(a.getBlockX() - b.getBlockX()) + 1;
    }

    public int getYDist() {
        if (a == null || b == null) {
            return 0;
        }

        return Math.abs(a.getBlockY() - b.getBlockY()) + 1;
    }

    public int getZDist() {
        if (a == null || b == null) {
            return 0;
        }
        return Math.abs(a.getBlockZ() - b.getBlockZ()) + 1;
    }
}